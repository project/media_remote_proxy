<?php

namespace Drupal\media_remote_proxy\OEmbed;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\media\OEmbed\ProviderRepository;
use Drupal\media\OEmbed\ProviderRepositoryInterface;
use Drupal\media_remote_proxy\ProxyConfigService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

/**
 * Overrides service class ProviderRepository to set proxy to httClient.
 */
class ProxyProviderRepository extends ProviderRepository implements ProviderRepositoryInterface {

  /**
   * Get access to the proxy settings.
   *
   * @var \Drupal\media_remote_proxy\ProxyConfigService
   *   The proxy config service.
   */
  protected ProxyConfigService $proxyConfig;

  /**
   * Sets proxy to httpClient and constructs a ProviderRepository instance.
   *
   * @param \Drupal\media_remote_proxy\ProxyConfigService $proxy_config
   *   The proxy config service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param null $key_value_factory
   *   The key-value store factory.
   * @param null $logger_factory
   *   The logger channel factory.
   * @param int $max_age
   *   (optional) How long the cache data should be kept. Defaults to a week.
   */
  public function __construct(
    ProxyConfigService $proxy_config,
    ClientInterface $http_client,
    ConfigFactoryInterface $config_factory,
    TimeInterface $time,
    $key_value_factory = NULL,
    $logger_factory = NULL,
    int $max_age = 604800
  ) {
    $this->proxyConfig = $proxy_config;
    if ($this->proxyConfig->getStatus()) {
      $httpClientDefaultConfig = $http_client->getConfig();
      $mediaRemoteProxyConfig = $proxy_config->getProxyConfig();
      $config = NestedArray::mergeDeep($httpClientDefaultConfig, $mediaRemoteProxyConfig);
      $http_client = new Client($config);
    }
    parent::__construct($http_client, $config_factory, $time, $key_value_factory, $logger_factory, $max_age);
  }

}
