<?php

namespace Drupal\media_remote_proxy\OEmbed;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\media\OEmbed\ProviderRepositoryInterface;
use Drupal\media\OEmbed\ResourceFetcher;
use Drupal\media\OEmbed\ResourceFetcherInterface;
use Drupal\media_remote_proxy\ProxyConfigService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

/**
 * Overrides original service class ResourceFetcher to set proxy to httClient.
 */
class ProxyResourceFetcher extends ResourceFetcher implements ResourceFetcherInterface {

  /**
   * Get access to the proxy settings.
   *
   * @var \Drupal\media_remote_proxy\ProxyConfigService
   *   The proxy config service.
   */
  protected ProxyConfigService $proxyConfig;

  /**
   * Sets proxy to httpClient and constructs a ResourceFetcher object.
   *
   * @param \Drupal\media_remote_proxy\ProxyConfigService $proxy_config
   *   The proxy config service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\media\OEmbed\ProviderRepositoryInterface $providers
   *   The oEmbed provider repository service.
   * @param \Drupal\Core\Cache\CacheBackendInterface|null $cache_backend
   *   The cache backend.
   */
  public function __construct(
    ProxyConfigService $proxy_config,
    ClientInterface $http_client,
    ProviderRepositoryInterface $providers,
    CacheBackendInterface $cache_backend = NULL) {
    $this->proxyConfig = $proxy_config;
    if ($this->proxyConfig->getStatus()) {
      $httpClientDefaultConfig = $http_client->getConfig();
      $mediaRemoteProxyConfig = $proxy_config->getProxyConfig();
      $config = NestedArray::mergeDeep($httpClientDefaultConfig, $mediaRemoteProxyConfig);
      $http_client = new Client($config);
    }
    parent::__construct($http_client, $providers, $cache_backend);
  }

}
