<?php

namespace Drupal\media_remote_proxy\OEmbed;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\media\OEmbed\ProviderRepositoryInterface;
use Drupal\media\OEmbed\ResourceFetcherInterface;
use Drupal\media\OEmbed\UrlResolver;
use Drupal\media\OEmbed\UrlResolverInterface;
use Drupal\media_remote_proxy\ProxyConfigService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

/**
 * Overrides original service class UrlResolver to set proxy to httClient.
 */
class ProxyUrlResolver extends UrlResolver implements UrlResolverInterface {

  /**
   * Get access to the proxy settings.
   *
   * @var \Drupal\media_remote_proxy\ProxyConfigService
   *   The proxy config service.
   */
  protected ProxyConfigService $proxyConfig;

  /**
   * Sets proxy to httpClient and constructs a UrlResolver object.
   *
   * @param \Drupal\media_remote_proxy\ProxyConfigService $proxy_config
   *   The proxy config service.
   * @param \Drupal\media\OEmbed\ProviderRepositoryInterface $providers
   *   The oEmbed provider repository service.
   * @param \Drupal\media\OEmbed\ResourceFetcherInterface $resource_fetcher
   *   The OEmbed resource fetcher service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Cache\CacheBackendInterface|null $cache_backend
   *   The cache backend.
   */
  public function __construct(
    ProxyConfigService $proxy_config,
    ProviderRepositoryInterface $providers,
    ResourceFetcherInterface $resource_fetcher,
    ClientInterface $http_client,
    ModuleHandlerInterface $module_handler,
    CacheBackendInterface $cache_backend = NULL
  ) {
    $this->proxyConfig = $proxy_config;
    if ($this->proxyConfig->getStatus()) {
      $httpClientDefaultConfig = $http_client->getConfig();
      $mediaRemoteProxyConfig = $proxy_config->getProxyConfig();
      $config = NestedArray::mergeDeep($httpClientDefaultConfig, $mediaRemoteProxyConfig);
      $http_client = new Client($config);
    }
    parent::__construct($providers, $resource_fetcher, $http_client, $module_handler, $cache_backend);
  }

}
