<?php

namespace Drupal\media_remote_proxy;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\media_remote_proxy\Form\ProxySettingsForm;

/**
 * Retrieve proxy settings.
 */
class ProxyConfigService implements ProxyConfigServiceInterface {

  /**
   * Proxy config settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   *    The immutable config.
   */
  protected ImmutableConfig $proxyConfig;

  /**
   * Constructs Proxy Config instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->proxyConfig = $config_factory->get(ProxySettingsForm::SETTINGS);
  }

  /**
   * {@inheritDoc}
   */
  public function getStatus(): bool {
    return (bool) $this->proxyConfig->get('status');
  }

  /**
   * {@inheritDoc}
   */
  public function getHttpProxyUrl(): string {
    return $this->proxyConfig->get('http');
  }

  /**
   * {@inheritDoc}
   */
  public function getHttpsProxyUrl(): string {
    return $this->proxyConfig->get('https');
  }

  /**
   * {@inheritDoc}
   */
  public function getUserAgent(): string {
    return $this->proxyConfig->get('user_agent');
  }

  /**
   * {@inheritDoc}
   */
  public function getUserAgentDefault(): string {
    return 'Drupal/' . \Drupal::VERSION . ' (+https://www.drupal.org/) ' .
      \GuzzleHttp\default_user_agent();
  }

  /**
   * {@inheritDoc}
   */
  public function getExceptions(): array {
    if (empty($this->proxyConfig->get('exceptions'))) {
      return [];
    }
    return explode(' ', $this->proxyConfig->get('exceptions'));
  }

  /**
   * {@inheritDoc}
   */
  public function getVerify(): bool {
    return (bool) $this->proxyConfig->get('verify');
  }

  /**
   * {@inheritDoc}
   */
  public function getTimeout(): float {
    return floatval($this->proxyConfig->get('timeout'));
  }

  /**
   * {@inheritDoc}
   */
  public function getProxyConfig(): array {
    $options = [
      'verify'  => $this->getVerify(),
      'timeout' => $this->getTimeout(),
      'proxy'  => [
        'http' => $this->getHttpProxyUrl(),
        'https' => $this->getHttpsProxyUrl(),
        'no' => $this->getExceptions(),
      ],
    ];
    if ($userAgent = $this->getUserAgent()) {
      $options['headers'] = [
        'User-Agent' => $userAgent,
      ];
    }
    return $options;
  }

}
