<?php

namespace Drupal\media_remote_proxy;

/**
 * Media Remote Proxy config service interface.
 */
interface ProxyConfigServiceInterface {

  /**
   * Proxy settings status.
   *
   * @return bool
   *   Return TRUE or FALSE.
   */
  public function getStatus(): bool;

  /**
   * The HTTP scheme proxy url.
   *
   * @return string
   *   Returns HTTP Url.
   */
  public function getHttpProxyUrl(): string;

  /**
   * The HTTPS scheme proxy url.
   *
   * @return string
   *   Returns HTTPS Url.
   */
  public function getHttpsProxyUrl(): string;

  /**
   * Get proxy user agent.
   *
   * A string which can be used by the proxy server
   * to identify connection requests.
   *
   * @return string
   *   Returns a string with the user agent.
   */
  public function getUserAgent(): string;

  /**
   * Default Drupal/Guzzle User Agent.
   *
   * @return string
   *   Returns a string with the default user agent.
   */
  public function getUserAgentDefault(): string;

  /**
   * Exceptions to access directly, not via proxy.
   *
   * @return array
   *   Returns an array with exceptions.
   */
  public function getExceptions(): array;

  /**
   * SSL certificate verification behavior.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  public function getVerify(): bool;

  /**
   * Total timeout of the request in seconds.
   *
   * @return float
   *   Returns a float with the timeout.
   */
  public function getTimeout(): float;

  /**
   * Get proxy settings used for requests.
   *
   * @return array
   *   Returns an array with the proxy config.
   */
  public function getProxyConfig(): array;

}
