<?php

namespace Drupal\media_remote_proxy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Media Remote Proxy settings for this site.
 */
class ProxySettingsForm extends ConfigFormBase {
  const SETTINGS = 'media_remote_proxy.proxy_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'media_remote_proxy_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::SETTINGS);
    // Status.
    $form['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#description' => $this->t('The status of the proxy settings.'),
      '#options' => [
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
      '#default_value' => $config->get('status') ? $config->get('status') : 0,
    ];
    // HTTP Scheme.
    $form['http'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HTTP Proxy Url'),
      '#description' => $this->t('Use this proxy with "http".'),
      '#default_value' => $config->get('http'),
    ];
    // HTTPS Scheme.
    $form['https'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HTTPS Proxy Url'),
      '#description' => $this->t('Use this proxy with "https".'),
      '#default_value' => $config->get('https'),
    ];
    // Description.
    $form['proxy_url_description'] = [
      '#type' => 'item',
      '#markup' => $this->t('You can provide proxy URLs that contain ' .
        'a scheme, username, and password. ' .
        'For example, "http://username:password@192.168.16.1:10".'),
    ];
    // User-Agent.
    $form['user_agent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Agent'),
      '#description' => $this->t('A string which can be used by the ' .
        'proxy server to identify connection requests. Leave empty to use ' .
        'Drupal/Guzzle: @user_agent', [
          '@user_agent' => 'Drupal/' . \Drupal::VERSION .
            ' (+https://www.drupal.org/) ' . \GuzzleHttp\default_user_agent(),
        ]
      ),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('user_agent'),
    ];
    // Exceptions.
    $form['exceptions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exceptions'),
      '#description' => $this->t('Specify exceptions using either IP ' .
        'addresses or hostnames. Enter one exception separated by space. ' .
        'Exceptions will be accessed directly, not via proxy.'),
      '#default_value' => $config->get('exceptions'),
      '#rows' => 2,
      '#resizable' => 'vertical',
    ];
    // Verify.
    $form['verify'] = [
      '#type' => 'select',
      '#title' => $this->t('Verify'),
      '#description' => $this->t('Describes the SSL certificate ' .
        'verification behavior of a request. See @link', [
          '@link' => 'https://docs.guzzlephp.org/en/stable/request-options.html?highlight=verify#verify',
        ]
      ),
      '#options' => [
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
      '#default_value' => $config->get('verify') ? $config->get('verify') : 0,
    ];
    // Timeout.
    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Float describing the total timeout of ' .
        'the request in seconds. Use 0 to wait indefinitely (the default behavior).'),
      '#min' => 0,
      '#step' => 0.01,
      '#default_value' => $config->get('timeout') ? $config->get('timeout') : 0,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config(self::SETTINGS)
      ->set('status', $form_state->getValue('status'))
      ->set('http', $form_state->getValue('http'))
      ->set('https', $form_state->getValue('https'))
      ->set('user_agent', $form_state->getValue('user_agent'))
      ->set('exceptions', $form_state->getValue('exceptions'))
      ->set('verify', $form_state->getValue('verify'))
      ->set('timeout', $form_state->getValue('timeout', 30))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
